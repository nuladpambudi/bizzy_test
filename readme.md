# Bizzy Test

> A node.js app to solve Bizzy test problem set.

## How to use

* Each folder represents a problem from the Bizzy Hiring_Problem_Set
* The problem description can be found in each folder as html file
* To run the solution, execute `node app` in each folder.
