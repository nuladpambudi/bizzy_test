'use strict';
const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var inputs = [];

rl.on('line', (input) => {
  if (input <= 10000 && input >= 1) {
    inputs.push(input);
  }
})

rl.on('close', () => {
  let outputs = processInput(inputs);
  printOutput(outputs);
})

function processInput(inputs) {
  let result = [];
  for (let i = 0; i < inputs.length; i++) {
    result.push(primeCount(inputs[i]));
  }
  return result;
}

function primeCount(number) {
  let primeStatus = [];
  let result = 0;

  for (let i = 2; i <= number; i++) {
    if (primeStatus[i - 1] == undefined) {
      primeStatus[i - 1] = true;
      result++;
      for (let j = i + i; j <= number; j += i) {
        primeStatus[j - 1] = false;
      }
    }
  }

  return result;
}

function printOutput(outputs) {
  for (let i = 0; i < outputs.length; i++) {
    console.log(outputs[i]);
  }
}


