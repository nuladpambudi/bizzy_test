'use strict';

const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var outputs = [];
var rateDictionary = " ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

rl.on('line', (input) => {
    outputs.push(calculateSuccessRate(input))
})

rl.on('close', () => {
    printOutput(outputs);
})

function calculateSuccessRate(input) {
    let inputArray = input.toUpperCase().split("");
    let rateResult = 0;
    for (let i = 0; i < inputArray.length; i++) {
        if (rateResult < 100) {
            rateResult += rateDictionary.indexOf(inputArray[i]);
        }
        if (rateResult > 100) {
            rateResult = 100;
        }
    }

    return rateResult;
}

function printOutput(outputs) {
    for (let i = 0; i < outputs.length; i++) {
        console.log(outputs[i] + "%");
    }
}
