'use strict';
const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var routeNumber;
var routeIndex;
var routes = [];
var outputs = [];

rl.on('line', (input) => {
    parseInput(input);
})

rl.on('close', () => {
    outputs = processInput(routes);
    printOutput(outputs);
})

function parseInput(input) {
    if (routeNumber == undefined) {
        if (input < 1) {
            routeNumber = 1;
        }
        else if (input > 500) {
            routeNumber = 500;
        }
        else {
            routeNumber = input;
        }
        routeIndex = 0;
    }
    else {
        let route = [];

        let rawInput = input.split(' ');
        let limit;
        for (let i = 0; i < rawInput.length; i++) {
            if (i == 0) {
                if (rawInput[i] < 1) {
                    limit = 1;
                }
                else if (rawInput[i] > 100) {
                    limit = 100;
                }
                else {
                    limit = rawInput[i];
                }
            }
            else {
                if (i <= limit) {
                    let checkpoint = {
                        'id': i,
                        'altitude': rawInput[i]
                    }
                    route.push(checkpoint);
                }
            }
        }

        if (routeIndex < routeNumber) {
            routes[routeIndex] = route;
            
            if (routeIndex == routeNumber - 1) {
                rl.close();
            }

            routeIndex++;
        }
    }
}

function processInput(routes) {
    for (let i = 0; i < routes.length; i++) {
        let output = routes[i];
        output.sort(function (a, b) {
            return a.altitude - b.altitude;
        });
        outputs[i] = output;
    }
    return outputs;
}

function printOutput(outputs) {
    for (let i = 0; i < outputs.length; i++) {
        let printCheckpoint = "";
        let routeName = i + 1;
        for(let j = 0; j < outputs[i].length; j++) {
            printCheckpoint += outputs[i][j].id + ", ";
        }
        printCheckpoint = printCheckpoint.substr(0, printCheckpoint.length -2);

        console.log("Route " + routeName + ": " + printCheckpoint);
    }
}