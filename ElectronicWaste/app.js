'use strict';
const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var wasteCount;
var wastes = [];
var wasteIndex;
var outputs = [];

rl.on('line', (input) => {
    parseInput(input);
})

rl.on('close', () => {
    outputs = processInput(wastes);
    printOutput(outputs);
})

function parseInput(input) {
    if (wasteCount == undefined) {
        wasteCount = input
        wasteIndex = 0;
    }
    else {
        if (wasteIndex < wasteCount) {
            if (input < 1) {
                wastes[wasteIndex] = 1;    
            }
            else if (input > 10000) {
                wastes[wasteIndex] = 10000;
            }
            else {
                wastes[wasteIndex] = input;
            }
            
            if (wasteIndex == wasteCount - 1) {
                rl.close();
            }
            wasteIndex++;
        }
    }
}

function processInput(wastes) {
    for (let i = 0; i < wastes.length; i++) {
        let output = calculateRecycleValue(wastes[i]);
        outputs[i] = output;
    }

    return outputs;
}

function calculateRecycleValue(value) {
    let price1 = Math.floor(value / 2);
    let price2 = Math.floor(value / 3);
    let price3 = Math.floor(value / 4);
    let priceSum = price1 + price2 + price3;

    if (priceSum > value) {
        return calculateRecycleValue(price1) + calculateRecycleValue(price2) + calculateRecycleValue(price3);
    }
    else {
        return value;
    }
}

function printOutput(outputs) {
    for (let i = 0; i < outputs.length; i++) {
        console.log(outputs[i]);
    }
}
