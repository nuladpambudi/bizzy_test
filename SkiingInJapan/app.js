'use strict';
const readline = require('readline')

var xCount, yCount, xInputPosition, prevXposition, prevYPosition;
var skiMap = [];
var possibleRoutes = [];
var routeCheckedFlags = [];

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.on('line', (input) => {
    parseInput(input);
})

rl.on('close', () => {
    let route = [];
    prevXposition = 0;
    prevYPosition = 0;
    findRoutes(route, 0, 0);
    let bestRoute = determineBestRoute(possibleRoutes);
    printRoutes(bestRoute);
})

function parseInput(input) {
    if (xCount == undefined && yCount == undefined) {
        let dimensions = input.split(" ");
        xCount = dimensions[0];
        yCount = dimensions[1];
        xInputPosition = 0;
    }
    else {
        if (xInputPosition < xCount) {
            let rowInput = input.split(" ");
            let row = [];
            let defaultCheckedRow = [];
            for (let y = 0; y < yCount; y++) {
                row[y] = rowInput[y];
                defaultCheckedRow[y] = false;
            }

            skiMap[xInputPosition] = row;
            routeCheckedFlags[xInputPosition] = defaultCheckedRow;
            if (xInputPosition == xCount - 1) {
                rl.close();
            }
            xInputPosition++;
        }
    }
}

function findRoutes(route, x, y) {
    let isEndOfRoute = true;
    let localRoute = [];
    localRoute.push(skiMap[x][y]);
    routeCheckedFlags[x][y] = true;

    // check North
    if ((x > 1) && (skiMap[x][y] > skiMap[x - 1][y])) {
        findRoutes(route.concat(localRoute), x - 1, y);
        isEndOfRoute = false;
    }
    // check South
    if ((x < xCount - 1) && (skiMap[x][y] > skiMap[x + 1][y])) {
        findRoutes(route.concat(localRoute), x + 1, y);
        isEndOfRoute = false;
    }
    // check West
    if ((y > 0) && (skiMap[x][y] > skiMap[x][y - 1])) {
        findRoutes(route.concat(localRoute), x, y - 1);
        isEndOfRoute = false;
    }
    // check East
    if ((y < yCount - 1) && (skiMap[x][y] > skiMap[x][y + 1])) {
        findRoutes(route.concat(localRoute), x, y + 1);
        isEndOfRoute = false;
    }

    if (isEndOfRoute) {
        possibleRoutes.push(route.concat(localRoute));
    }

    if ((prevXposition + 1) * (prevYPosition + 1) < (xCount * yCount)) {
        let newRoute = [];
        if (prevYPosition < yCount - 1) {
            prevYPosition++;
        }
        else if (prevXposition < xCount - 1) {
            prevYPosition = 0;
            prevXposition++;
        }
        if (!routeCheckedFlags[prevXposition][prevYPosition]) {
            findRoutes(newRoute, prevXposition, prevYPosition);
        }
    }
}

function determineBestRoute(possibleRoutes) {
    let sortedRoutes = possibleRoutes;
    sortedRoutes.sort(function (a, b) {
        if (b.length != a.length) {
            return b.length - a.length;
        }
        else {
            return (b[0] - b[b.length - 1]) - (a[0] - a[a.length - 1]);
        }
    });
    return sortedRoutes[0];
}

function printRoutes(bestRoute) {
    let singlePrint = "";
    for (let j = 0; j < bestRoute.length; j++) {
        singlePrint += bestRoute[j];
        if (j < bestRoute.length - 1) {
            singlePrint += "-"
        }
    }
    console.log(singlePrint);
}

